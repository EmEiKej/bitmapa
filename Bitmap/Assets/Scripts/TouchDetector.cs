using UnityEngine;

public class TouchDetector : MonoBehaviour
{
    [SerializeField] GamePanel gamePanel;
    private void Update()
    {
        OnMouseClick();
    }
    void OnMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(mousePosition, Vector2.zero);
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.GetComponent<Pixel>() != null)
                {
                    hit.collider.gameObject.GetComponent<Pixel>().ChangeColor();
                    gamePanel.UpdateFinalBitmap(hit.collider.gameObject.GetComponent<Pixel>());
                }
            }
        }
    }
}

