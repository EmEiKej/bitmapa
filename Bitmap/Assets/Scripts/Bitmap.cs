using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bitmap : MonoBehaviour
{
    public Pixel[,] pixels = new Pixel[10, 10];
    public Color color;
    public void DetermineColor(Color color)
    {
        this.color = color;
    }
    public void AddPixel(Pixel pixel, int j, int k)
    {
        pixels[j, k] = pixel;
    }
    public void RemoveBoxColiderFromPixels()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                Destroy(pixels[i, j].gameObject.GetComponent<BoxCollider2D>());
            }
        }
    }
}
