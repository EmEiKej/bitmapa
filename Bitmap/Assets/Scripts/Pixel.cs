using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pixel : MonoBehaviour
{
    [SerializeField] public SpriteRenderer myColor;
    Color color;
    public void InitColor(Color color)
    {
        myColor.color = color;
        this.color = color;
    }
    public void ChangeColor()
    {
        if (color == myColor.color)
        {
            myColor.color = Color.black;
        }
        else
        {
            myColor.color = color;
        }

    }
}
