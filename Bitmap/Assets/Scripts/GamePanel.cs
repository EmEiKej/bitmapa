using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class GamePanel : MonoBehaviour
{
    [SerializeField] GameObject pixel;
    [SerializeField] GameObject bitmap;
    List<Bitmap> bitmaps = new List<Bitmap>();
    Vector2 bitmapPosition = new Vector2(-750, 0);
    Vector2 bitmapMove = new Vector2(500, 0);
    Vector2 pixelPosition = new Vector2(-148.5f, 148.5f);
    int pixelMove = 33;
    List<Color> colors = new List<Color> { Color.red, Color.green, Color.blue, Color.white };
    int amountOfPixels = 10;
    private void Start()
    {
        InitPixels();
    }
    void InitPixels()
    {
        for (int i = 0; i < 4; i++)
        {
            GameObject temptBitmap = Instantiate(bitmap, this.transform);
            temptBitmap.transform.localPosition = bitmapPosition + bitmapMove * i;
            bitmaps.Add(temptBitmap.GetComponent<Bitmap>());
            bitmaps.Last().DetermineColor(colors[i]);
            for (int j = 0; j < amountOfPixels; j++)
            {
                for (int k = 0; k < amountOfPixels; k++)
                {
                    GameObject temptPixel = Instantiate(pixel, bitmaps.Last().gameObject.transform);
                    temptPixel.transform.localPosition = pixelPosition + new Vector2(pixelMove*k,-pixelMove*j);
                    bitmaps.Last().AddPixel(temptPixel.GetComponent<Pixel>(), j, k);
                    bitmaps.Last().pixels[j, k].InitColor(bitmaps.Last().color);
                }
            }
        }
        bitmaps.Last().RemoveBoxColiderFromPixels();
    }
    public void UpdateFinalBitmap(Pixel pixel)
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                for (int k = 0; k < 10; k++)
                {
                    if (pixel == bitmaps[i].pixels[j, k]) 
                    {
                        bitmaps[3].pixels[j, k].myColor.color = bitmaps[0].pixels[j, k].myColor.color +
                            bitmaps[1].pixels[j, k].myColor.color + bitmaps[2].pixels[j, k].myColor.color;
                    }
                }
            }
        }
    }
}
